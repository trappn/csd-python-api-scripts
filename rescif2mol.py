###############################################################
### CONVERTS A DIRECTORY OF CIF AND RES FILES TO MOL2       ###
### (AND STRIPS ORIGINAL FILENAME FROM MOL COMMENT)         ###
###############################################################

from os import listdir, getcwd
from os.path import isfile, join, splitext

from ccdc import io

# current path:
mypath = getcwd()
# found files:
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]

for item in files:
    filename, file_ext = splitext(item)
    if file_ext == '.res':
        mol_reader = io.MoleculeReader(item, format='res')
    elif file_ext == '.cif':
        mol_reader = io.MoleculeReader(item, format='cif')
    else:
        continue
    outfile = filename + '.mol2'
    print('converting: ' + item + '  --->  ' + outfile)
    with io.MoleculeWriter(join(mypath, outfile)) as mol_writer:
        for molecule in mol_reader:
            mol_writer.write(molecule)
    # kill comment line (for anonymization):
    with open(join(mypath, outfile), 'r') as file:
        # re-read:
        data = file.readlines()
        # now change 2nd line, mind the newline:
        data[1] = 'converted from ' + file_ext + ' file.\n'
    # write everything back
    with open(join(mypath, outfile), 'w') as file:
        file.writelines(data)
