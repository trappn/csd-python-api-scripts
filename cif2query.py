###############################################################
### CHECKS A DIRECTORY OF CIF AND RES FILES TO LOOK FOR     ###
### MATCHING CSD ENTRIES (BASED ON UC & SUM FORMULA)        ###
###############################################################

from os import listdir, getcwd
from os.path import isfile, join, splitext

from ccdc import io
from ccdc.search import ReducedCellSearch, SimilaritySearch

# current path:
mypath = getcwd()
# found files:
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
published = []
unpublished = []

for item in files:
    filename, file_ext = splitext(item)
    if file_ext == '.res':
        crystal_reader = io.CrystalReader(item, format='res')
    elif file_ext == '.cif':
        crystal_reader = io.CrystalReader(item, format='cif')
    else:
        continue
    for cryst in crystal_reader:
        query = ReducedCellSearch.Query(cryst.cell_lengths,
            cryst.cell_angles,
            cryst.lattice_centring)
        searcher = ReducedCellSearch(query)
        #searcher = SimilaritySearch(cryst)
        hits = searcher.search()
        count = 0
        if len(hits) > 0:
            for i in hits:
                if i.entry.formula == cryst.formula:
                    count = count + 1
        if count > 0:
            #print(item + ' (' + cryst.formula + ') : ' + str(count) + ' hits')
            published.append(item)         
        else:
            #print(item + ' (' + cryst.formula + ') : unpublished')
            unpublished.append(item)   

# cast published list to dict to remove duplicates:
published = list(dict.fromkeys(published))
unpublished = list(dict.fromkeys(unpublished))

print('\nPublished #   : ' + str(len(published)))
print('Unpublished # : ' + str(len(unpublished)))

print('\n<UNPUBLISHED DATA BELOW>\n')
for i in unpublished:
    print(i) 