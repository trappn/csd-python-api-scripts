#!/usr/bin/env python
import sys, argparse

import argparse
parser = argparse.ArgumentParser(
        description='Converts refcode list in .gcd format to CCDC numbers.',
        epilog="Input file must be generated in Conquest (Export Entries as...->Refcode: CSD entry identifier list)")
parser.add_argument("inputfile")
parser.add_argument("outputfile")
args = parser.parse_args()
ifile = args.inputfile
ofile = args.outputfile

from ccdc.io import EntryReader
csd_reader = EntryReader('CSD')
index_list = []
with open(ifile) as inp:
    for line in inp:
        if len(line) > 5:   # if line contains refcode
            index_list.append(line)

with open(ofile, 'w') as outp:
    for x in index_list:
        output = csd_reader.entry(x)
        outp.write(str(output.ccdc_number) + '\n')
