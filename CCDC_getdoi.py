#!/usr/bin/env python
import sys, argparse

import argparse
parser = argparse.ArgumentParser(
        description='Generates DOI table in .csv format.',
        epilog="Input file must be generated in Conquest (Export Parameters and Data->Spreadsheet (Field Separator=Comma))")
parser.add_argument("inputfile")
parser.add_argument("outputfile")
args = parser.parse_args()
ifile = args.inputfile
ofile = args.outputfile

from ccdc.io import EntryReader
csd_reader = EntryReader('CSD')
index_list = []
s = ";"  # chosen separator, ; = csv format
with open(ifile) as inp:
    next(inp) # ignore first line = header
    for line in inp:
        parts = line.split(',') # split line into parts
        if len(parts) > 1:   # if at least 2 parts/columns
            index_list.append(parts[1]) # extract 2nd column = CSD code   

with open(ofile, 'w') as outp:
    for x in index_list:
        output = csd_reader.entry(x)
        output_con = (output.identifier,str(output.ccdc_number),output.formula,output.chemical_name,str(output.publication.doi))
        outp.write(s.join(output_con).encode('utf-8') + '\n')
